(function ($) {
$(document).ready(function () {

$(".playset").each(function () {
    var context = this;
    var scrubber = $(".scrubber", context);
    var thumb = $(".thumb", context);
    var inpoint = $(".inpoint", context);
    var outpoint = $(".outpoint", context);
    var playable = $(".playable", context);

    var scrubbing, playing_on_scrub_start;
    function position_to_time (controlelt) {
        var l = parseInt(controlelt.css("left"));
        var p = (l / (scrubber.width() - controlelt.width()));
        var d = playable.playable("duration") || 3600;
        // console.log("p", p, "d", d);
        return p*d;
    }
    function time_to_position (ct, controlelt) {
        var d = playable.playable("duration") || 3600;
        var l = (ct/d) * (scrubber.width() - controlelt.width());
        return Math.floor(l);
    }

    // Respond to events to update interface (optional)
    playable.bind('play', function () {
        scrubber.addClass("playing");
        $(".play", context).text("pause");
    }).bind('pause', function () {
        scrubber.removeClass("playing");
        $(".play", context).text("play");
    }).bind("durationchange", function () {
        var d = playable.playable("duration");
        $(".duration", context).text(d.secondsTo("mm:ss"))
    }).bind("timeupdate", function () {
        var ct = $(this).playable("currentTime");
        $(".currenttime", context).text(ct.secondsTo("mm:ss"))
        if (!scrubbing) {
            var d = $(this).playable("duration") || 3600;
            var l = (ct/d) * (scrubber.width() - thumb.width());
           thumb.css("left", time_to_position(ct, thumb)+"px");
        }
    }).bind("waiting", function () {
        scrubber.addClass("waiting");
        $(".status", context).text("waiting");
    }).bind("ready", function () {
        scrubber.removeClass("waiting");
        $(".status", context).text("");
    });

    $(".thumb", context).click(function () {
        if (!scrubbing) {
            // console.log("click");
            playable.playable("toggle");
        }
    }).draggable({
        containment: "parent",
        start: function () {
            // console.log("start");
            scrubbing = true;
            playing_on_scrub_start = playable.playable("playing");
            playable.playable("pause");
        },
        drag: function () {
            playable.playable("currentTime", position_to_time(thumb));
        },
        stop: function () {
            setTimeout(function () { scrubbing = false; }, 10);
            if (playing_on_scrub_start) { playable.playable("play"); }
        }
    });
    $(".inpoint", context).draggable({
        containment: "parent",
        drag: function () {
            var t = position_to_time(inpoint);
            // console.log("setting startTime", t.secondsTo("mm:ss"));
            playable.playable("startTime", t);
        }
    });
    $(".outpoint", context).draggable({
        containment: "parent",
        drag: function () {
            var t = position_to_time(outpoint);
            // console.log("setting endTime", t.secondsTo("mm:ss"));
            playable.playable("endTime", t);
        }
    });

});

});
})(jQuery);
