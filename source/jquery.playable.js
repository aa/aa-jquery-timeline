(function ($) {

var SHOW_CONTROLS = false;

/**
 * @fileoverview 
 * @author Michael Murtaugh <mm@automatist.org> and the Active Archive contributors
 * @license GNU AGPL
 * @requires ...

//////////////////////////////
PLAYABLE
//////////////////////////////

Goals of playable:
* Abstraction around different media APIs: html5, youtube, vimeo, ...
* In addition to standard HTML5 style interface: start time / end time
* Support for deferred playback (super lightweight screen presence, just-in-time instantiation of heavy stuff)
* Flexible "preview" states (text, poster, ...)
* Support for Text display / caption.
* Multi-format support with live switching

FUTURE:
* Multi-format
* Deal with options (and allow for data-OPTION attributes)

Source_sniffers are the actual plugins that manage the audio/video elements.

*/

/* Timecode code from jquery.media
 * 2011. Created by Oscar Otero (http://oscarotero.com / http://anavallasuiza.com)
*/



function playable (elt, opts) {
    var that = {};
    var currentTime = 0,
        duration,
        playing = false,
        ready = false,
        sources = [],
        current_source,
        current_source_loaded = false;
    var poll_lasttime, got_duration = false;
    var poll_interval_time = 250;

    var startTime, endTime;
    // var hasEnded = true;
    var END_STATE_NO_END_TIME = 0;
    var END_STATE_SEEKING = 1;
    var END_STATE_WAITING_FOR_END = 2;
    var END_STATE_ENDED = 3;
    var end_state = END_STATE_NO_END_TIME;


    function select_source (opts) {
        // opts = opts || {};
        // console.log("select_source", source_sniffers.length);
        for (var i=0; i<source_sniffers.length; i++) {
            var sniffer = source_sniffers[i];
            var obj = sniffer(elt, opts);
            if (obj) {
                current_source = obj;
                // console.log("set source", obj);
                // current_source.load(elt, opts);
                break;
            }
        }
        if (!current_source) $.error("playable, no source selected");
    }
    select_source(opts);

    /* Event Handlers */
    // events that will be emitted by the .playable element:
    // play
    // pause
    // durationchange
    $(elt).bind("durationchange", function () {
        duration = current_source.duration();
    }).bind("waiting", function () {
        // console.log("playable receives waiting");
        ready = false;
    }).bind("ready", function () {
        // console.log("playable receives ready");
        ready = true;
    });
    // timeupdate

    that.duration = function () {
        if (current_source) return current_source.duration();
    }

    /* Methods */
    that.play = function () {
        playing = true;
        // console.log("play", current_source, current_source_loaded);
        if (current_source) {
            if (!current_source_loaded) {
                current_source.load(elt, {autoplay: true, starttime: currentTime});
                current_source_loaded = true;
            } else {
                current_source.play();
            }
        }
    }
    var onEndCallback;
    that.playpart = function (opts) {
        // console.log("playpart", opts);
        endTime = opts.end;
        // console.log("END_STATE_SEEKING");
        end_state = END_STATE_SEEKING;
        ready = false; // do this in currentTime (seek)
        onEndCallback = opts.onend;
        that.currentTime(opts.start);
        that.play();
    }

    that.playing = function () {
        return playing;
    }

    that.pause = function () {
        playing = false;
        if (current_source) current_source.pause();
    }

    that.toggle = function () {
        // console.log("playable.toggle", playing);
        if (playing) { that.pause(); } else { that.play(); }
    }

    that.currentTime = function (t) {
        if (t === undefined) {
            // return that.timeline.getCurrentTime();
            return currentTime;
        } else {
            // console.log("playable.currentTime", t);
            currentTime = t;
            if (current_source) current_source.currentTime(t);
        }
    }

    function poll_time () {
        var ct;
        if (current_source) {
            ct = current_source.currentTime();
        } else {
            ct = currentTime;
        }
        // console.log("poll_time", ct);
        /* end time */
        // console.log("*", ct, endTime, end_state);

        
        if (end_state == END_STATE_SEEKING && ct < endTime) {
            // END_STATE_SEEKING means we are waiting until we are before the end time
            // console.log("END_STATE_WAITING_FOR_END");            
            end_state = END_STATE_WAITING_FOR_END;
        }
        // if (end_state == END_STATE_WAITING_FOR_END && ct >= endTime) {
        if (playing && ready && endTime && ct >= endTime) {
        // if (endTime !== null && !hasEnded && ct >= endTime) {
            console.log("playable: end detection");
            that.pause();
            // hasEnded = true;
            end_state = END_STATE_ENDED;
            $(elt).trigger("ended");
            // console.log("ended");
            if (onEndCallback) {
                var cb = onEndCallback;
                onEndCallback = null;
                cb();
            }
        }
        /*
         else if (endTime !== null && hasEnded && ct < endTime) {
            // log("playable, restart detected");
            hasEnded = false;
            // this._trigger("play");
        }
        */

        if (ct !== poll_lasttime) {
            currentTime = ct;
            // console.log("triggering timeupdate");
            $(elt).trigger("timeupdate", undefined, {time: ct});
        }
        poll_lasttime = ct;

        // try getting duration                
        if (!got_duration) {
            duration = that.duration();
            if (duration) {
                got_duration = true;
                $(elt).trigger("durationchange");
            }
        }
    }

    window.setInterval(poll_time, poll_interval_time);

    that.startTime = function (t) {
        if (t === undefined) {
            return startTime;
        } else {
            startTime = t;
        }
    }
    that.endTime = function (t) {
        if (t === undefined) {
            return endTime;
        } else {
            endTime = t;
        }
    }


    /////////////////////////////////////////////////
    /// SYNCWITH

    var sync = [];

    that.syncwith = function (elt) {
        elt = $(elt);        
        // console.log("syncwith", that, elt);
        var target = elt.playable().data("playable");
        var targetelt = elt.get(0);
        console.log("target", target);
        sync.push(target);
        $(targetelt).bind("play", function () {
            that.play();
        }).bind("pause", function () {
            that.pause();
        }).bind("timeupdate", function () {
            var ct = target.currentTime();
            if ( (ct > (currentTime + 1)) || (ct < (currentTime - 1))) {
                that.currentTime(ct);
            }
        });
    }
    return that;
}

var source_sniffers = [];

// Support NICE SCRUBBABLE Preview states

String.prototype.startsWith = function(str) {
    return (this.toLowerCase().match("^"+str) !== null)
}
String.prototype.endsWith = function (str) {
    return (this.toLowerCase().match(str+"$") !== null)
}

function html5 (elt, opts) {
    // console.log("playable.source html5:", elt);
    var that = {};
    var href = $(elt).attr("data-src") || "";
    var mediatag;

    /* Core methods: load, unload, play, pause, currentTime, duration */

    var type = opts.type || $(elt).attr("data-type") || "";
    // console.log("html5", type);
    var ok = false, audio = false;
    if ((type === "audio/ogg") || (type==="audio/mp3")) {
        audio = true;
        ok = true;
    } else if ((type==="video/ogg") || (type==="video/mp4") || (href.endsWith(".ogv")) || (href.endsWith(".ogg")) ) {
        ok = true;
    }
    if (!ok) return;

    that.load = function (appendelt, opts) {

        var tagstr = "<";
        tagstr += audio ? "audio" : "video";
        if (opts.autoplay) {
            tagstr += " autoplay";
        }
        if (SHOW_CONTROLS) { tagstr += " controls"; }
        tagstr += ">";
        tagstr += "Audio"
        tagstr += "</";
        tagstr += audio ? "audio" : "video";
        tagstr += ">";

        mediatag = $(tagstr).attr("src", href).get(0);
        // console.log(mediatag);
        if (opts.starttime) {
            $(mediatag).bind("loadedmetadata", function () {
                // console.log("html, seek on start", opts.starttime, mediatag);
                mediatag.currentTime = opts.starttime;
            });
        }
        /* Events */
        $(mediatag).bind("durationchange", function () {
            $(elt).trigger("durationchange");
        }).bind("play", function () {
            // console.log("play");
            $(elt).trigger("play");
        }).bind("pause", function () {
            $(elt).trigger("pause");
        }).bind("timeupdate", function () {
            // console.log("HTML5 timeupdate event", arguments);
            $(elt).trigger("timeupdate");
        }).bind("loadstart", function () {
            // console.log("loadstart");
            $(elt).trigger("waiting");
        }).bind("canplay", function () {
            // console.log("canplay");
            $(elt).trigger("ready");
        }).bind("seeking", function () {
            $(elt).trigger("waiting");
        }).bind("seeked", function () {
            $(elt).trigger("ready");
        });

        $(mediatag).appendTo(appendelt);
    };

    that.unload = function () {
        mediatag.remove();
        mediatag = null;
    }
    that.play = function () {
        // should play trigger a load?
        if (mediatag) mediatag.play();
    }
    that.pause = function () {
        if (mediatag) mediatag.pause();
    }
    that.currentTime = function (t) {
        if (t === undefined) {
            return mediatag ? mediatag.currentTime : undefined;
        } else {
            // SETTER
            if (mediatag) {
                try {
                     mediatag.currentTime = t;
                } catch (e) {}
            }
        }
    }
    that.duration = function () {
        if (mediatag) return mediatag.duration;
    }

    return that;
}

// boilerplate jquery plugin method dispatch code
$.fn.playable = function(method) {
    // pull out the playable object from the given element, and apply the given method
    var returnvalue = null;
    var args = arguments;

    this.each(function () {
        var elt = this;
        var obj = $(elt).data("playable");
        if (obj === undefined) {
            // initialize
            var opts = {};
            if (typeof(method) == "object") {
                opts = method;
            }
            // console.log("playable.init", elt, opts);
            obj = playable(elt, opts)
            $(elt).data("playable", obj);
        }
        if (method && typeof(method) === "string") {
            if (obj[method]) {
            // console.log("playable.dispatching", args);
                returnvalue = obj[method].apply(this, Array.prototype.slice.call(args, 1));
            } else {
                $.error("playable has no method " + method);
            }
        }
    });

    if (returnvalue !== null) {
        return returnvalue;
    } else {
        return this;
    }
        
    /*
    if (methods[method]) {
        return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || ! method) {
        return methods.init.apply(this, arguments);
    } else {
        $.error('Method ' +  method + ' does not exist on jQuery.playable');
    }
    */
};

$.fn.playable.register_source_sniffer = function (s) { source_sniffers.push(s); }

$.fn.playable.register_source_sniffer(html5);


})(jQuery);


/**
 * $media (core) jQuery plugin (v.1.2)
 *
 * 2011. Created by Oscar Otero (http://oscarotero.com / http://anavallasuiza.com)
 *
 * $media is released under the GNU Affero GPL version 3.
 * More information at http://www.gnu.org/licenses/agpl-3.0.html
 */
/**
 * function String.toSeconds ()
 *
 * Convert any number to seconds
 */
String.prototype.toSeconds = function () {
	var time = this;

	if (/^([0-9]{1,2}:)?[0-9]{1,2}:[0-9]{1,2}(\.[0-9]+)?(,[0-9]+)?$/.test(time)) {
		time = time.split(':', 3);

		if (time.length == 3) {
			var ms = time[2].split(',', 2);
			ms[1] = ms[1] ? ms[1] : 0;

			return ((((parseInt(time[0], 10) * 3600) + (parseInt(time[1], 10) * 60) + parseFloat(ms[0])) * 1000) + parseInt(ms[1], 10)) / 1000;
		}

		var ms = time[1].split(',', 1);
		ms[1] = ms[1] ? ms[1] : 0;

		return ((((parseInt(time[0], 10) * 60) + parseFloat(ms[0])) * 1000) + parseInt(ms[1], 10)) / 1000;
	}

	return parseFloat(time).toSeconds();
}


/**
 * function String.secondsTo (outputFormat)
 *
 * Convert a seconds time value to any other time format
 */
String.prototype.secondsTo = function (outputFormat) {
	return this.toSeconds().secondsTo(outputFormat);
}


/**
 * function Number.toSeconds ()
 *
 * Convert any number to seconds
 */
Number.prototype.toSeconds = function () {
	return Math.round(this * 1000) / 1000;
}


/**
 * function Number.secondsTo (outputFormat)
 *
 * Convert a seconds time value to any other time format
 */
Number.prototype.secondsTo = function (outputFormat) {
	var time = this;

	switch (outputFormat) {
		case 'ms':
			return Math.round(time * 1000);

		case 'mm:ss':
		case 'hh:mm:ss':
		case 'hh:mm:ss.ms':
			var hh = '';

			if (outputFormat != 'mm:ss') {
				hh = Math.floor(time / 3600);
				time = time - (hh * 3600);
				hh += ':';
			}

			var mm = Math.floor(time / 60);
			time = time - (mm * 60);
			mm = (mm < 10) ? ("0" + mm) : mm;
			mm += ':';

			var ss = time;

			if (outputFormat == 'hh:mm:ss' || outputFormat == 'mm:ss') {
				ss = Math.round(ss);
			}
			ss = (ss < 10) ? ("0" + ss) : ss;

			return hh + mm + ss;
	}

	return time;
};


