(function($) {

function voidplayer (elt, opts) {
    var that = {};
    var playing = false, currentTime = 0;
    var interval_id = null;
    var interval_time = 250;

    /* Core methods: load, unload, play, pause, currentTime, duration */

    var type = opts.type || $(elt).attr("data-type") || "";
    if (type !== "void") return;

    that.load = function (appendelt, opts) {
        if (opts.starttime) {
            currentTime = opts.starttime;
        }
        if (opts.autoplay) {
            that.play();
        }
    };
    that.unload = function () {};

    that.play = function () {
        // console.log("voidplayer: play");
        if (!interval_id) {
            // console.log("voidplayer: start");
            $(elt).trigger("play");
            start = new Date().getTime() - (currentTime * 1000);
            interval_id = window.setInterval(function () {
                var now = new Date();
                that.currentTime((now-start)/1000);
                // $(elt).trigger("timeupdate");
            }, interval_time);
        }
    }
    that.pause = function () {
        // console.log("voidplayer: pause");
        if (interval_id) {
            $(elt).trigger("pause");
            window.clearInterval(interval_id)
            interval_id = null;
        }
    }
    that.currentTime = function (t) {
        if (t === undefined) {
            return currentTime;
        } else {
            // SETTER
            currentTime = t;
            start = new Date().getTime() - (currentTime * 1000);
        }
    }
    that.playing = function () {
        return (interval_id !== null);
    }
    that.duration = function () {
        return;
    }

    return that;
}


$.fn.playable.register_source_sniffer(voidplayer);


})(jQuery);
