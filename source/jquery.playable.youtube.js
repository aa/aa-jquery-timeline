/**
 * jquery.ui.youtube.js
 *
 * part of ActiveArchives, activearchives.org
 *
 * a youtube jquery plugin
 * part of the aa "playable" suite
 *
 * Depends:
 *   swfobject.js
 *   jquery.ui.widget.js VERSION >=1.8!
 *   jquery.timecode.js

youtube playstates:
    unstarted (-1),
    ended (0),
    playing (1),
    paused (2),
    buffering (3),
    video cued (5)
 
quality levels:
    small: Player height is 240px, and player dimensions are at least 320px by 240px for 4:3 aspect ratio.
    medium: Player height is 360px, and player dimensions are 640px by 360px (for 16:9 aspect ratio) or 480px by 360px (for 4:3 aspect ratio).
    large: Player height is 480px, and player dimensions are 853px by 480px (for 16:9 aspect ratio) or 640px by 480px (for 4:3 aspect ratio).
    hd720: Player height is 720px, and player dimensions are 1280px by 720px (for 16:9 aspect ratio) or 960px by 720px (for 4:3 aspect ratio).
    hd1080: Player height is 1080px, and player dimensions are 1920px by 1080px (for 16:9 aspect ratio) or 1440px by 1080px (for 4:3 aspect ratio).
    highres: Player height is greater than 1080px, which means that the player's aspect ratio is greater than 1920px by 1080px.
    default: YouTube selects the appropriate playback quality. This setting effectively reverts the quality level to the default state and nullifies any previous efforts to set playback quality using the cueVideoById, loadVideoById or setPlaybackQuality functions.

**/

// youtube's chromeless player API requires globally defined callbacks
aa_youtube_players_by_id = {}
function onYouTubePlayerReady(playerid) { aa_youtube_players_by_id[playerid]._youtubeReady(); }
/*
function onYouTubeStateChange (state, playerid) { aa_youtube_players_by_id[playerid]._youtubeStateChange(state); }
function onYouTubePlaybackQualityChange (quality, playerid) { aa_youtube_players_by_id[playerid]._youtubePlaybackQualityChange(quality); }
*/

(function ($, undefined) {

// http://stackoverflow.com/questions/786380/using-youtubes-javascript-api-with-jquery
var closureFaker = function (func, scope) {
    var functionName = 'closureFake' + (((1+Math.random())*0x10000000)|0).toString(16);
    window[functionName] = function () {
        func.apply(scope || window, arguments);
    };
    // console.log('created function:', functionName, window[functionName]);
    return functionName;
};

DEBUG = false;

function log () {
    try { console.log.apply(console, arguments); } catch (e) {}
}

var urlpats = [
    new RegExp("youtube\\.com/.+[?=]v=([\\w-]+)"),
    new RegExp("youtube\\.com/.+/(\\w+)$"),
    new RegExp("youtu\\.be/([\\w-]+)")
]

function sniff (url) {
    for (var i=0; i<urlpats.length; i++) {
        if (urlpats[i].test(url)) return true;
    }
};
function id_from_url (url) {
    for (var i=0; i<urlpats.length; i++) {
        var results = urlpats[i].exec(url);
        if (results) { return results[1]; }
    }
}

var aa_youtube_id = 0;


var options = {
    autoplay: false,
    autobuffer: false,
    autohide: true, // flowplayer
    controls: false,
    width: 640,
    height: 360,
    fpScaling: 'fit',
    preventDefault: true,
    timeUpdateInterval: 1000 // determines frequency of timeupdate event callbacks (ms)
};

function youtube (elt, opts) {
    var that = {};
    var src = $(elt).attr("data-src");
    var youtube_id, player_id, chromless;
    /* Core methods: load, unload, play, pause, currentTime, duration */
    var controls = false;
    var start, end, hasEnded = false;

    var type = opts.type || $(elt).attr("data-type") || "";

    var accept = (type === "youtube") || sniff(src);
    if (!accept) return;
    // console.log("youtube", src);
    that.load = function (appendelt, opts) {
        if (opts.starttime !== undefined) {
            start = $.timecode_tosecs_attr(opts.starttime);
            if (opts.endtime) {
                end = $.timecode_tosecs_attr(opts.endtime);
            }
        }

        youtube_id = id_from_url(src);
        // console.log("src", src, "id", youtube_id);
        player_id = "aayoutube"+(++aa_youtube_id); // NB: avoid "_" in ID! -- evil bug observed with unusual URL encoding sometimes happening
        // console.log("player_id", player_id);
        var params = { allowScriptAccess: "always", allowfullscreen: "true", wmode: "opaque" };
        var atts = { id: player_id };
        chromeless = !controls;
        var swfurl = chromeless ?
            "http://www.youtube.com/apiplayer?enablejsapi=1&version=3&rel=0&playerapiid=" + player_id : 
            "http://www.youtube.com/v/"+youtube_id+"?version=3&enablejsapi=1&rel=0&playerapiid=" + player_id;
        aa_youtube_players_by_id[player_id] = that;     
        $("<div></div>").attr("id", player_id+"swfobject").appendTo(appendelt);
        swfobject.embedSWF(swfurl, player_id+"swfobject", options.width, options.height, "8", null, null, params, atts);
        //_youtubeReady will be called next, when the plugin has finished loading...
    }

    that.currentTime = function (t) {
        if (t === undefined) {
            try {
                var p = document.getElementById(player_id);
                return p.getCurrentTime();
            } catch (e) { if (DEBUG) log("youtube.getCurrentTime failed"); }
        } else {
            // log("playable, seek", t);
            t = $.timecode_tosecs_attr(t);
            try { document.getElementById(player_id).seekTo(t, true); }
            catch (e) { if (DEBUG) log("youtube.seek failed"); }
            /* now done in _polltime
            if (o._end !== null && o._hasEnded) {
                o._hasEnded = false;
            }
            */
        }
    };

    that.play = function () {
        // console.log("youtube.play");
        var o = this.options;
        if (end !== null && hasEnded) {
            // log("youtube: restarting");
            this.seek(start);
            // o._hasEnded = false;
            // needs to happen somewhere else...
        }
        try { document.getElementById(player_id).playVideo(); }
        catch (e) { if (DEBUG) log("youtube.play failed"); }
    },

    that.pause = function () {
        try { document.getElementById(player_id).pauseVideo(); }
        catch (e) { if (DEBUG) log("youtube.pause failed"); }
    };

    that.playing = function () {
        try { return document.getElementById(player_id).getPlayerState() === 1 }
        catch (e) { if (DEBUG) log("youtube.playing failed"); }
    };

    that.duration = function () {
        try { return document.getElementById(player_id).getDuration(); }
        catch (e) { if (DEBUG) log("youtube.duration failed"); }
    };

    that._youtubeReady = function () {
        // console.log("youtube.ready");
        var player = document.getElementById(player_id);

        // Register for PlayerStateChange Events
        if (player) {
            // forget about passing an anonymous function to youtube... old-school hack to the rescue!
            // http://stackoverflow.com/questions/786380/using-youtubes-javascript-api-with-jquery
            // MM: May 2011, Added false as it seems to be required for (at least) FF4
            // this hack stopped working 2012, changed to closureFaker
            // var callbackstr = '(function(state) { return onYouTubeStateChange(state, "'+player_id+'"); })';
            // player.addEventListener("onStateChange", callbackstr, false);
            // callbackstr = '(function(state) { return onYouTubePlaybackQualityChange(state, "'+player_id+'"); })';
            // player.addEventListener("onPlaybackQualityChange", callbackstr, false);

            player.addEventListener("onStateChange", closureFaker(function(state) {
                that._youtubeStateChange(state);
            }, this));
            /*
            player.addEventListener("onPlaybackQualityChange", closureFaker(function(state) {
                this._youtubePlaybackQualityChange(state);
            }, this));
            */
        }

        // Set initial playstate
        if (chromeless) {
            // console.log("initial playstate", player, youtube_id);
            autoplay = true;
            if (autoplay) {
                player.loadVideoById(youtube_id, start);
            } else {
                player.cueVideoById(youtube_id, start);
            }
        } else {
            if (start) { player.seekTo(start, true); }
            if (autoplay) { player.playVideo(); } else { player.pauseVideo(); }
        }
                
        // this._trigger("ready");
        $(elt).trigger("ready");
    };

    that._youtubeStateChange = function (newstate) {
        // console.log("youtube statechange", newstate);
        if (newstate === 1) {
            // console.log("youtube.trigger play", elt);
            $(elt).trigger("play");
        } else {
            $(elt).trigger("pause");
        }
        if (newstate == 0) {
            if (DEBUG) { log("youtube statechange: clip ended"); }
            // this._trigger("ended");
            $(elt).trigger("ended");
        }
    };

    return that;

}


$.fn.playable.register_source_sniffer(youtube);


/*
$.widget("ui.youtube", {


    _init: function () {
	    // log("init");
        var o = this.options;
        if (o.preventDefault) {
    	    this.element.click(function (e) { e.preventDefault(); })
    	}

    },

    getVideoBytesLoaded: function () {
        try { return document.getElementById(this.options.player_id).getVideoBytesLoaded(); }
        catch (e) { if (DEBUG) log("youtube.getVideoBytesLoaded failed"); }
    },
    getVideoStartBytes: function () {
        try { return document.getElementById(this.options.player_id).getVideoStartBytes(); }
        catch (e) { if (DEBUG) log("youtube.getVideoStartBytes failed"); }
    },
    getVideoBytesTotal: function () {
        try { return document.getElementById(this.options.player_id).getVideoBytesTotal(); }
        catch (e) { if (DEBUG) log("youtube.getVideoBytesTotal failed"); }
    },
    mute: function () {
        try { document.getElementById(this.options.player_id).mute(); }
        catch (e) { if (DEBUG) log("youtube.mute failed"); }
    },
    unMute: function () {
        try { document.getElementById(this.options.player_id).unMute(); }
        catch (e) { if (DEBUG) log("youtube.unMute failed"); }
    },
    isMuted: function () {
        return document.getElementById(this.options.player_id).isMuted();
    },
    setVolume: function (v) {
        // v: 0 to 100
        document.getElementById(this.options.player_id).setVolume();
    },
    getVolume: function () {
        // returns: 0 to 100 (even if muted)
        return document.getElementById(this.options.player_id).getVolume();
    },
    getPlaybackQuality: function () {
        return document.getElementById(this.options.player_id).getPlaybackQuality();
    },
    setPlaybackQuality: function (level) {
        document.getElementById(this.options.player_id).setPlaybackQuality(level);
    },
    getAvailableQualityLevels : function () {
        return document.getElementById(this.options.player_id).getAvailableQualityLevels();
    },
    
    destroy: function () {
	    // log("destroy");
        var o = this.options;
		this.element.removeClass( "aa-youtube" );
        delete aa_youtube_players_by_id[o.player_id];
        $("#"+o.player_id).remove();
        if (o._pollintervalid) {
            window.clearInterval(o._pollintervalid);
            o._pollintervalid = null;
        }
		$.Widget.prototype.destroy.apply( this, arguments );
    },

    _trigger: function (evt) {
        if (evt === "ended") {
            // console.log("playable: ended!");
            // trigger an actual DOM event on the element
            this.element.trigger("ended");
        }
        $.Widget.prototype._trigger.apply( this, arguments );
    },


    _youtubePlaybackQualityChange : function (quality) {
        if (DEBUG) { log("onPlaybackQualityChange", quality); }
    }

});
*/

})(jQuery);

